package com.wimonsiri.mydiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.wimonsiri.mydiary.databinding.FragmentFeelPageBinding

class FeelPage : Fragment() {
    private var binding: FragmentFeelPageBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentFeelPageBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            feelpage  = this@FeelPage
        }
    }

    fun SaveToback(){
        findNavController().navigate(R.id.homeFragment)
    }

}