package com.wimonsiri.mydiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.wimonsiri.mydiary.databinding.FragmentWriteDiaryBinding


class WriteDiary : Fragment() {
    private var binding: FragmentWriteDiaryBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentWriteDiaryBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
           writediary  = this@WriteDiary
        }
    }

    fun SaveToback(){
        findNavController().navigate(R.id.homeFragment)
    }

}