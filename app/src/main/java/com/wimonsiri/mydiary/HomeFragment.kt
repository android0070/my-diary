package com.wimonsiri.mydiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.wimonsiri.mydiary.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private var binding: FragmentHomeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentHomeBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            homefragment = this@HomeFragment
        }
    }

    fun goTonextScreen(){
        findNavController().navigate(R.id.action_homeFragment_to_writeDiary)
    }

    fun goTonextScreenfeel(){
        findNavController().navigate(R.id.action_homeFragment_to_feelPage)
    }
    fun goTonextScreenhistory(){
        findNavController().navigate(R.id.action_homeFragment_to_historyFragment)
    }


}